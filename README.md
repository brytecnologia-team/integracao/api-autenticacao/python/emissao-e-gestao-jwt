# Gestão de validade da credencial de acesso

Este projeto apresenta uma abordagem de gerenciamento do tempo de vida das credenciais emitidas pela plataforma da BRy ([BRy Cloud]).

A abordagem apresenta um gerenciamento ativo, onde uma tarefa realiza validações períodicas do período de vigência do access token armazenado em memória.

Abaixo seguem os passos principais:
* Ao iniciar (dispare uma request POST no endpoint "/jwt/iniciar/"), a aplicação exemplo requisita um **Access Token** para a plataforma de serviços da BRy, utilizando as credenciais de uma aplicação cadastrada: **client_id** e **client_secret**.  
* Em um período configurável, a tarefa desperta e realiza a validação do tempo de vida do Access Token (processo de decodifição do JWT e extração da claim **exp**).
* Sempre que o Access Token estiver próximo de sua expiração, a tarefa dispara o evento de renovação da credencial utilizando o **Refresh Token**.

### Tech

O exemplo utiliza das bibliotecas Python abaixo:
* [Requests] - HTTP for Humans!
* [Flask] - Flask (source code) is a Python Web framework designed to receive HTTP requests.
* [Flask-restful] - Flask-RESTful is an extension for Flask that adds support for quickly building REST APIs
* [Flask-Cors] - A Flask extension for handling Cross Origin Resource Sharing (CORS), making cross-origin AJAX possible.
* [Python] - Python 3.8
* [PyJwt] - JSON Web Token Python implementation
* [Flask-APScheduler] - Flask-APScheduler is a Flask extension which adds support for the APScheduler.

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com os dados de conexão e identificação da aplicação cliente.

| Variável | Descrição | Arquivo |
| ------ | ------ | ------ |
| client_id | **client_id** de uma aplicação. | gestao_credenciais.py
| client_secret | **client_secret** de uma aplicação. | gestao_credenciais.py
| check_interval | Define o intervalo entre checagens da validade do token para a renovação, quando necessário, em segundos | gestao_credenciais.py

### Observações

Os atributos de data e hora codificados nos tokens JWT (access token e refresh token), seguem o fuso horário UTC-0.
Lembrando que o Brasil possui 4 fusos horários:
* UTC-2 - Atol das Rocas e Fernando de Noronha.
* UTC-3 **(oficial)** - Estados não citados nos demais itens.
* UTC-4 - Amazonas, Mato Grosso, Mato Grosso do Sul, Rondônia e Roraima.
* UTC-5 - Acre e alguns municípios do Amazonas.

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. Utilizamos o Python versão 3.8 e pip3 para a instalação das dependências.

Comandos:

Instalar Flask:

    -pip3 install Flask

Instalar Flask-Cors

    -pip3 install -U flask-cors

Instalar Flask-Restful:

    -pip3 install flask-restful

Instalar Flask-APScheduler:

    -pip install Flask-APScheduler

Instalar JSON Web Token Python:

    -pip install jwt

Executar programa:

    -python3 gestao_credenciais.py



   [Requests]: <https://pypi.org/project/requests/>
   [Flask]: <https://palletsprojects.com/p/flask/>
   [Flask-restful]:<https://flask-restful.readthedocs.io/en/latest/> 
   [Flask-Cors]: <https://flask-cors.readthedocs.io/en/latest/>
   [Python]: <https://www.python.org/downloads/release/python-380/>
   [PyJwt]: <https://pyjwt.readthedocs.io/en/latest/>
   [Flask-APScheduler]: <https://github.com/viniciuschiele/flask-apscheduler/>

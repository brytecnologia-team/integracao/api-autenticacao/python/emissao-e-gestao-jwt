import calendar
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_cors import CORS
import requests
import jwt
from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime

app = Flask(__name__)
CORS(app)
api = Api(app)

# Define o intervalo entre checagens da validade do token para a renovação, quando necessário, em segundos
check_interval = 60 # 1 minuto para cada checking como default

# client_id de uma aplicação no KMS
client_id = 'client_id'

# client_secret de uma aplicação no KMS
client_secret = 'client_secret'

# Token sendo atualmente utilizado para autenticar (deixe vazio ao iniciar a aplicação e solicite a geração de um novo para popular esta variável)
global token_atual
token_atual = ''

# Token sendo atualmente guardado para renovar o token_atual quando este estiver proximo do momento de expiração, deixe vazio também
global refresh_token
refresh_token = ''

scheduler = BackgroundScheduler()

class auto_renova(Resource):
    def post(self):
        global token_atual
        global refresh_token

        decode = jwt.decode(token_atual, options={"verify_signature": False}, algorithms="RS256", audience=["kms", "account"])
        token_time = decode.get('exp') # Pega o claim que guarda a data/hora de expiração a partir do payload do token

        current_datetime = datetime.utcnow()

        current_timetuple = current_datetime.utctimetuple()

        current_timestamp = calendar.timegm(current_timetuple) # O timestamp UNIX UTC exato do momento atual

        # Compara o timestamp do token em memoria com o do momento atual em UTC (Universal Time) para validá-lo
        response = None
        if token_time <= current_timestamp + (check_interval * 2):

            print("Solicitando a renovação do Token...")
            response = requests.post('http://localhost:8000/jwt/renovatoken') # Irá solicitar a renovação no endpoint de renovar tokens
 
            return response.json() # A resposta com o token de acesso renovado, ou a mensagem de erro se existir algum
        else:
            print("Tempo remanescente para utilização do token atual: " + str(round(((token_time - current_timestamp)/60))) + " minutos.")              
            
            

class iniciar_aplicacao(Resource):
    def post(self):
        # Pega o conteúdo que vem da requisição do front-end
        data = request.form

        client_data = {
                'client_id': client_id,
                'client_secret': client_secret,
                'grant_type': "client_credentials"
            }
        header = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }

        print('============= Gerando token JWT com os dados fornecidos... =============')

        response = requests.post('https://cloud.bry.com.br/token-service/jwt',
                                 data=client_data, headers=header)
        if response.status_code == 200:
            print('Emissao de token JWT concluida com sucesso')
            
            global token_atual
            token_atual = response.json()['access_token']
            
            global refresh_token
            refresh_token = response.json()['refresh_token']

            # Inicia a tarefa que fica rodando no background para periodicamente solicitar uma checagem para renovação do token JWT
            scheduler.add_job(func=requests.post, trigger="interval", seconds=check_interval, args=['http://localhost:8000/jwt/autorenova', None, {'Content-Type': 'application/x-www-form-urlencoded'} ])
            scheduler.start()

            return response.json()['access_token']
        else:
            print(response.text)

class renova_token(Resource):
    def post(self):

        global token_atual
        global refresh_token

        client_data = {
                'refresh_token': refresh_token,
                'grant_type':   'refresh_token'
            }
        header = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }

        print('============= Renovando token JWT com os dados fornecidos... =============')
        
        response = requests.post('https://cloud.bry.com.br/token-service/jwt',
                                 data=client_data, headers=header)
        if response.status_code == 200:
            print('Token JWT Renovado com Sucesso')

            token_atual = response.json()['access_token']
            refresh_token = response.json()['refresh_token']

            return response.json()['access_token']
        else:
            print(response.text)


api.add_resource(iniciar_aplicacao, '/jwt/iniciar') # Rodar apenas este endpoint, uma vez, para iniciar a auto renovação
api.add_resource(renova_token, '/jwt/renovatoken')
api.add_resource(auto_renova, '/jwt/autorenova')

if __name__ == '__main__':
    app.run(host="localhost", port=8000, debug=True)
